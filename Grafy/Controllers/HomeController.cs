﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Grafy.Models;

namespace Grafy.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var viewGraph = GetViewModelGraph(10);
            return View(viewGraph);
        }

        [HttpPost]
        public IActionResult CreateNewGraph(int vertexCount)
        {
            var viewGraph = GetViewModelGraph(vertexCount);
            return View("Index", viewGraph);
        }

        private ViewModelGraph GetViewModelGraph(int vertexCount)
        {
            var maxAllowedSubstraction = 4;
            var popsize = 100;
            var graph = new Graph();
            graph.CreateMatrix(vertexCount);
            graph.CutMatrix();
            var population = new Population[50];
            var pareto = new List<List<int?>>();
            for (var k = 0; k < 50; k++)
            {
                pareto.Add(new List<int?>());
                population[k] = new Population(graph.Matrix.GetLength(0), popsize, maxAllowedSubstraction);
                population[k].CreatePopulation();
                if (k != 0)
                {
                    population[k].Populations[0] =
                        new UnitOfPopulation(population[k - 1].Populations[0].Chromosome, vertexCount);
                    population[k].Populations[1] =
                        new UnitOfPopulation(population[k - 1].Populations[1].Chromosome, vertexCount);
                    population[k].Populations[2] =
                        new UnitOfPopulation(population[k - 1].Populations[2].Chromosome, vertexCount);
                    population[k].Populations[3] =
                        new UnitOfPopulation(population[k - 1].Populations[3].Chromosome, vertexCount);
                    population[k].Populations[4] =
                        new UnitOfPopulation(population[k - 1].Populations[4].Chromosome, vertexCount);
                    population[k].Populations[5] =
                        new UnitOfPopulation(population[k - 1].Populations[5].Chromosome, vertexCount);
                    population[k].Populations[6] =
                        new UnitOfPopulation(population[k - 1].Populations[6].Chromosome, vertexCount);
                    population[k].Populations[7] =
                        new UnitOfPopulation(population[k - 1].Populations[7].Chromosome, vertexCount);
                    population[k].Populations[8] =
                        new UnitOfPopulation(population[k - 1].Populations[8].Chromosome, vertexCount);
                    population[k].Populations[9] =
                        new UnitOfPopulation(population[k - 1].Populations[9].Chromosome, vertexCount);
                    var p =
                        population[k - 1].Populations.OrderBy(x => x.Chromosome[vertexCount]).First();
                    population[k].Populations[10] = new UnitOfPopulation(p.Chromosome, vertexCount);
                }

                foreach (var chromosome in population[k].Populations)
                {
                    Population.F1F2(graph.Matrix, chromosome);
                }

                population[k].SortByF2AndF1();
                var activeElement = 0;
                foreach (var item in population[k].Populations)
                {
                    var betterInf1 = true;
                    var betterInf2 = true;
                    for (var i = 0; i < activeElement; i++)
                    {
                        if (item.Chromosome[population[k].Populations[i].Chromosome.Count - 2] >= population[k]
                                .Populations[i].Chromosome[population[k].Populations[i].Chromosome.Count - 2])
                        {
                            betterInf1 = false;
                        }

                        if (item.Chromosome[population[k].Populations[i].Chromosome.Count - 1] >= population[k]
                                .Populations[i].Chromosome[population[k].Populations[i].Chromosome.Count - 1])
                        {
                            betterInf2 = false;
                        }
                    }

                    if (betterInf1 || betterInf2)
                    {
                        pareto[k].Add(item.Chromosome[item.Chromosome.Count - 2]);
                    }
                    else
                    {
                        pareto[k].Add(null);
                    }

                    activeElement++;
                }
            }


            var listOfVertexInFirstGraph = new List<int>();
            for (var i = 0; i < population[0].Populations[0].Chromosome.Count; i++)
            {
                if (i % 2 == 1)
                {
                    listOfVertexInFirstGraph.Add(i + 1);
                }
            }

            var viewGraph = new ViewModelGraph(graph.Matrix, listOfVertexInFirstGraph)
            {
                Pupulation = population, Pareto = pareto
            };


            var theBestF1 = new List<int>();
            var theBestF2 = new List<int>();

            foreach (var t in population)
            {
                var minF1 = 5000;
                var minF2 = 5000;
                for (var j = 0; j < popsize; j++)
                {
                    if (t.Populations[j].Chromosome[vertexCount] < minF1)
                    {
                        minF1 = t.Populations[j].Chromosome[vertexCount];
                    }

                    if (t.Populations[j].Chromosome[vertexCount + 1] < minF2)
                    {
                        minF2 = t.Populations[j].Chromosome[vertexCount + 1];
                    }
                }

                theBestF1.Add(minF1);
                theBestF2.Add(minF2);
            }

            viewGraph.TheBestF1 = theBestF1;
            viewGraph.TheBestF2 = theBestF2;

            return viewGraph;
        }
    }
}