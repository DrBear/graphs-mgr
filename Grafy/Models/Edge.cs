﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Grafy.Models
{
    public class Edge
    {
        private List<int> VertexID { get; set; }
        private float Weight { get; set; }
        public Edge(int firstVertex, int secondVertex, float weight)
        {
            VertexID = new List<int>() { firstVertex, secondVertex };
            Weight = weight;
        }        
    }
}
