﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Grafy.Models
{
    public class Graph
    {
        public int IDGraph { get; set; }
        public List<Vertex> Vertex { get; set; }
        public int [,] Matrix { get; set; }
        public void CutMatrix()
        {
            for (int i = 0; i < Matrix.GetLength(0); i++)
            {
                for (int j = i; j < Matrix.GetLength(1); j++)
                {
                    Matrix[i, j] = Matrix[j, i];
                }
            }

        }
        public void CreateMatrix(int CountVertex)
        {
            var matrix = new int[CountVertex, CountVertex];
            for (int i = 1; i < CountVertex; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    matrix[i, j] = RandomIfExist();
                }
            }
            this.Matrix=matrix;
        }
        public void SaveEdgesToTxt()
        {

            var matrixText = MatrixToString(Matrix);
            File.WriteAllText(@"..\..\Graf.txt", matrixText);
        }
        public string MatrixToString(int[,] matrix)
        {
            var text = "";
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                text += "| ";
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    text += matrix[i, j];
                }
                text += " |\n";

            }
            return text;
        }
        public int RandomIfExist()
        {
            Random random = new Random();
            if (random.Next(2)==0)
            {
                return 0;
            }
            return(random.Next(9)+1);
        }
    }
}
