﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace Grafy.Models
{
    public class ViewModelGraph
    {
       public int [,] GraphMatrix;
       public List<int> IdVectorsInFirstGraph;
        public  Population[] Pupulation;
        public List<List<int?>> Pareto;
        public List<int> TheBestF1;
        public List<int> TheBestF2;


        public int VertexCount;
        public ViewModelGraph(int[,] matrix, List<int> listOfVertexInFirstGraph)
        {
            this.GraphMatrix = matrix;
            this.IdVectorsInFirstGraph = listOfVertexInFirstGraph;
        }
    }
}
