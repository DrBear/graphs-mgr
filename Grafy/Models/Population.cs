﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grafy.Models
{
    public class Population
    {
        public List<UnitOfPopulation> Populations { get; private set; }
        private readonly int _popSize;
        private readonly int _maxVertexDifference;
        private readonly int _countOfVertex;
        public Population(int countOfVertex, int popSize, int maxVertexDifference)
        {
            _countOfVertex = countOfVertex;
            _popSize = popSize;
            _maxVertexDifference = maxVertexDifference;
        }
        public void CreatePopulation()
        {
            Populations = new List<UnitOfPopulation>(_popSize);
            for (var i = 0; i < _popSize; i++)
            {
                var chromosomeValue = new List<int>(_countOfVertex + 2);
                CreateIndividual(chromosomeValue);
                Populations.Add(new UnitOfPopulation(chromosomeValue));
            }
        }

        private void CreateIndividual(List<int> chromosomeValue)
        {
            var rand = new Random();
            do
            {
                chromosomeValue.Clear();
                for (var j = 0; j < _countOfVertex; j++)
                {
                    chromosomeValue.Add(rand.Next(2));
                }
            } while (Math.Abs(chromosomeValue.Count(x => x == 1) - chromosomeValue.Count(x => x == 0)) >= _maxVertexDifference);
        }
        public static void F1F2(int[,] matrix, UnitOfPopulation individual)
        {
            var countEdges = 0;
            var sumOfWeights = 0;
            for (var i = 0; i < individual.Chromosome.Count; i++)
            {
                if (individual.Chromosome[i] != 1) continue;
                for (var j = 0; j < individual.Chromosome.Count; j++)
                {
                    if (individual.Chromosome[j] != 0) continue;
                    if (!IsEdgeConnector(matrix, i, j)) continue;
                    countEdges++;
                    sumOfWeights += matrix[i, j];
                }
            }
            individual.Chromosome.Add(countEdges);
            individual.Chromosome.Add(sumOfWeights);
        }
        public void SortByF2AndF1()
        {
            var indexOfF1 = Populations[0].Chromosome.Count - 2;
            var indexOfF2 = Populations[0].Chromosome.Count - 1;
            Populations = Populations.OrderBy(x => x.Chromosome[indexOfF2]).ThenBy(f=>f.Chromosome[indexOfF1]).ToList();
        }
        private static bool IsEdgeConnector(int[,] matrix, int i, int j)
        {
            return matrix[i, j] != 0;
        }
    }
}
