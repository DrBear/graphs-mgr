﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Grafy.Models
{
    public class UnitOfPopulation
    {
        private float X { get; set; }
        private float Y { get; set; }
        public readonly List<int> Chromosome;
        public int Generation { get; set; }

        public UnitOfPopulation(List<int> chromosome)
        {
            Chromosome = chromosome;
        }

        public UnitOfPopulation(IReadOnlyList<int> chromosome, int countOfChromosome)
        {
            var list = new List<int>();
            for (var i = 0; i < countOfChromosome; i++)
            {
                list.Add(chromosome[i]);
            }

            Chromosome = list;
        }

        public void Mutate()
        {
            var rand = new Random();
            var geneToChange = rand.Next(0, Chromosome.Count - 2);
            Chromosome[geneToChange] = Chromosome[geneToChange] == 1 ? 0 : 1;
        }
    }
}