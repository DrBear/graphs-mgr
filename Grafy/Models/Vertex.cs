﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace Grafy.Models
{
    public class Vertex
    {
        private int Id { get; set; }
        private List<Edge> Edges { get; set; }
        public int Degree => Edges.Count();
        public int OwnerIDGraph { get; set; }

        private Vertex(int id,List<Edge> edges) {
            Id = id;
            Edges = new List<Edge>() ;
            Edges = edges;          
        }
        public Vertex RandomVertex(int [,] matrix,int IDForNewVertex) {
            var edges = new List<Edge>();     
                ReadEdges(matrix, IDForNewVertex);           
            var vertex = new Vertex(IDForNewVertex, edges);
            return vertex;
        }

        private static List<Edge> ReadEdges(int [,] matrix,int numberOfVertexToRead)
        {
            var edges = new List<Edge>();
            var random = new Random();
            for (var i = 0; i < matrix.GetLength(0); i++) edges.Add(new Edge(numberOfVertexToRead,i, random.Next(1,10)));
            return edges;
        }
    }
}
